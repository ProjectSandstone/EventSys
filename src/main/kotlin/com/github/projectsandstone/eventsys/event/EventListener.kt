/*
 *      EventImpl - Event implementation generator written on top of CodeAPI
 *
 *         The MIT License (MIT)
 *
 *      Copyright (c) 2017 ProjectSandstone <https://github.com/ProjectSandstone/EventImpl>
 *      Copyright (c) contributors
 *
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a copy
 *      of this software and associated documentation files (the "Software"), to deal
 *      in the Software without restriction, including without limitation the rights
 *      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *      copies of the Software, and to permit persons to whom the Software is
 *      furnished to do so, subject to the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included in
 *      all copies or substantial portions of the Software.
 *
 *      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *      THE SOFTWARE.
 */
package com.github.projectsandstone.eventsys.event

/**
 * Listen for an [Event].
 *
 * When [Event] is dispatched in [EventManager], [EventListener.onEvent] is called in its order.
 * (see [EventPriority]).
 *
 * This class can generated via [com.github.projectsandstone.eventsys.gen.event.EventGenerator.createMethodListener]
 */
interface EventListener<in T : Event> : Comparable<EventListener<*>> {

    /**
     * Called when the [event] is dispatched in [EventManager].
     *
     * @param event Event
     * @param owner Owner of the [event].
     */
    fun onEvent(event: T, owner: Any)

    /**
     * Priority of event, this priority will be used to sort [EventListener] in listener collection.
     */
    val priority: EventPriority
        get() = EventPriority.NORMAL

    /**
     * Phase where this listener listen.
     *
     * @see ListenerSpec.phase
     */
    val phase: Int
        get() = 0

    /**
     * Ignore if event is cancelled.
     */
    val ignoreCancelled
        get() = false

    override fun compareTo(other: EventListener<*>): Int {
        val compare = this.priority.compareTo(other.priority)

        return if (compare == 0) -1 else compare
    }
}